classdef BATS_Code < handle
    %% Batched Sparse code

    properties(SetAccess=private,GetAccess=public)
        T % Number of symbols
        K % Number of input packets
        M % Size of batches
        p1 % Order of galois field for outer code
        p2 % Order of galois field for inner code
        delta % Parameter for PRNG
        c % Parameter for PRNG
        seed % Seed for PRNG
        ic_density %Inner code density (for sparse matrix)
        prng
    end
    properties(Access=private)
        decoder %BP decoder
    end
    properties(Constant) %Default values
        DEFAULT_MAX_BATCHES=10000;
        DEFAULT_OUTER_CODE_ORDER=8;
        DEFAULT_INNER_CODE_ORDER=1;
        DEFAULT_IC_DENSITY=0.4;
    end

    methods(Access=private)
        function obj = BATS_Code(K,M,p1,p2,density,delta,c,seed)
            %% Parameter initialization
            obj.K=K;
            obj.M=M;
            obj.p1=p1;
            obj.p2=p2;
            obj.ic_density=density;
            obj.delta=delta;
            obj.c=c;
            obj.seed=seed;
            obj.decoder=0;
            %% Init the pseudo random number generator
            % Used to select batch degree, collaborator blocks and random
            % generator matrix
            obj.prng=PRNG(K,delta,c,seed,M);
        end
    end
    methods(Static)
      function newobj = instance(K,M,varargin)
          %% Create a new instance of BATS Code
          persistent uniqueInstance
          if isempty(uniqueInstance)
              %Object does not exist yet and must be instantiated

              %%Input arguments control
              narginchk(2,8)
              if nargin<3
                  p1=BATS_Code.DEFAULT_OUTER_CODE_ORDER;
              else
                  p1=varargin{1};
              end
              if nargin<4
                  p2=BATS_Code.DEFAULT_INNER_CODE_ORDER;
              else
                  p2=varargin{2};
              end
              if nargin<5
                  ic_density=BATS_Code.DEFAULT_IC_DENSITY;
              else
                  ic_density=varargin{3};
              end
              if nargin<6
                  delta=PRNG.DEFAULT_DELTA;
              else
                  delta=varargin{4};
              end
              if nargin<7
                  c=PRNG.DEFAULT_C;
              else
                  c=varargin{5};
              end
              if nargin<8
                  seed=PRNG.DEFAULT_SEED;
                  %seed=randi(65535);
              else
                  seed=varargin{6};
              end

              %Instantiation
              newobj = BATS_Code(K,M,p1,p2,ic_density,delta,c,seed);
              uniqueInstance = newobj;
          else
              %Object already exists
              narginchk(0,0);
              newobj = uniqueInstance;
          end
      end
    end
    methods
        function [batch]=Encode(obj,in_pkts)
            %% Encode 'in_pkts' using BATS codes
            % Apply both outer and inner coding, according to the defined
            % parameters.

            %Compute size of input data
            [obj.T,obj.K]=size(in_pkts);

            % Increase the seed of the batch
            obj.seed=obj.seed+1;
            obj.prng.SetSeed(obj.seed);

            %% Outer coding
            % Get degree of batch, indexes of collaborator packets and
            % generator matrix
            [d,indexes,Gmat]=obj.prng.GetGmatrix(2^obj.p1,obj.M);
            enc_data=in_pkts(:,indexes)*Gmat;

            G=Gmat;

            Coeff=eye(obj.M);
            Coeff=gf(Coeff,obj.p1);
            batch=Batch(obj.M,obj.seed,indexes,G,Coeff,enc_data);
        end

        function [is_done,num_decoded]=Decode(obj,batch)
            %%Wrapper for decoder
                if obj.decoder==0
                    obj.decoder=BP_Decoder(obj.K);
                end
                batch.Gmat=batch.Gmat*batch.Coeff;
                batch.rank=rank(batch.Gmat);
                [is_done,num_decoded]=obj.decoder.AddBatch(batch);
        end

        function message=GetMessage(obj)
                message=obj.decoder.decoded;
        end

        function [batch]=Recode(obj,batch,varargin)
            narginchk(2,3)
            if nargin==2
                Phi=randi([0 2.^obj.p2-1],obj.M,obj.M);
                Phi=gf(Phi,obj.p1);
                batch.Coeff=batch.Coeff*Phi;
                batch.data=batch.data*Phi;
            elseif nargin==3
                Phi=randi([0 2.^obj.p2-1],r,varargin{1});
                Phi=gf(Phi,obj.p1);
                batch.Coeff=batch.Coeff*Phi;
                batch.data=batch.data*Phi;
                obj.M=varargin{1};
            end
        end

    end
end
