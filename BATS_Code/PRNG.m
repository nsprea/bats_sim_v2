classdef PRNG <handle
  %% PRNG: Pseudo-Random Number Generator according to RSD distribution.
  % It is used to determine the DEGREE of the encoded block, i.e. the
  % number of source blocks (contributors) which will be XOR'ed to
  % generate the encoded block.


  properties(Access=private)
    seed    % Seed for PRNG
    K       % Number of blocks which compose the file
    delta
    c
    cdf     % RSD cumulative distribution function
    M       % BATS size
    end

    properties(Constant)
    DEFAULT_DELTA=0.05;
    DEFAULT_C=1;
    DEFAULT_SEED=1234;
  end

  methods
    function obj = PRNG(K,delta,c,seed,M)
      %% Class constructor

      if nargin<4
        seed=obj.DEFAULT_SEED;
      end
      obj.seed=seed;
      rng(seed,'twister');
      obj.K=K;
      obj.M=M;

      fileid=fopen('BATS_Code/simDegreeK1600M32m8.txt');
      pdf=fscanf(fileid,'%e ');
      cdf=cumsum(pdf);

      switch M
      case 1
        obj.cdf=cdf(1:floor(3200/(20)):end);
        obj.cdf(1)=[];
      case 4
        obj.cdf=cdf(1:floor(3200/(200)):end);
      case 8
        obj.cdf=cdf(1:floor(3200/(500)):end);
      case 16
        obj.cdf=cdf(1:floor(3200/(1000)):end);
      otherwise
        obj.cdf=cdf(1:floor(3200/K):end);
      end
      fclose(fileid);
    end

    function SetSeed(obj,seed)
      %% Set seed for PRNG
      obj.seed=seed;
      rng(seed,'twister');
    end

    function [d,indexes]=GetBlocks(obj)
      %% Returns the 'indexes' of a set of 'd' blocks
      % randomly sampled i = 1, ..., K-1 uniformly, where d is
      % sampled accordingly to the RSD.
      d=obj.Pick_d();
      indexes=randperm(obj.K,d);
    end

    function [d, indexes, Gmat]=GetGmatrix(obj,q,M)
      %% Returns the 'indexes' of a set of 'd' packets and a 'Gmat',
      % generator matrix for the outer code. The generator matrix is a
      % random matrix, whose elements are picked from a finite field GF(q)

      [d,indexes]=obj.GetBlocks();
      Gmat=gf(randi([0 q-1],d,M),log2(q));

    end

  end
  methods(Access=private)
    function d=Pick_d(obj)
      %% Pick a random d, following the RSD
      p=rand(); % Pick a random number between [0,1]
      % Find the first value in CDF, whose probability is greater
      % than p. The index of that value is the desired d.
      d=find(obj.cdf>p,1,'first');
      if isempty(d)||(d>obj.K)%(d>obj.M)
        d=obj.K;%obj.M;
      end
    end

  end
end
