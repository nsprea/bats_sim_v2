classdef BP_Decoder<handle
    %% Belief Propagation decoder
    %   Iterative algorithm for decoding BATS codes.
    %   Decode a batch if its rank is equal to its degree. Pass the decoded
    %   message to other batches.
    
    properties
        batches %List of batches
        decoded %Decoded packets
        decoded_indexes %Indexes of decoded packets
        decoded_bool_indexes
        num_input_pkts %Number of packets to be decoded
    end
    
    methods
        function obj=BP_Decoder(num_input_pkts)
            %%Class constructor
            obj.num_input_pkts=num_input_pkts;
            obj.decoded_bool_indexes=zeros(1,num_input_pkts,'logical');
        end 
        
        function [is_done,num_decoded]=AddBatch(obj,batch)
            %If batch is immediately decodable, decode it!
            if batch.isDecodable
                [B,indexes]=batch.Decode();
                obj.decoded(:,indexes)=B.x; %Add to decoded pkts
                
                %Update indexes of decoded packets
                obj.decoded_bool_indexes(indexes)=1;
                obj.decoded_indexes=find(obj.decoded_bool_indexes);
                
                %Remove decoded packets from list of contributors of other
                %batches
                zero_degree_batches=[];
                for i=1:numel(obj.batches)
                    obj.batches(i).RemoveContributors(B.x,indexes);
                    if obj.batches(i).degree==0
                        zero_degree_batches=[zero_degree_batches, i];
                    end
                end
                obj.batches(zero_degree_batches)=[];
                
                
                %Decode batches recursively
                decodable=obj.FindOneDecodable();
                while decodable
                        [B,indexes]=obj.batches(decodable).Decode(); %Decode batch
                        obj.batches(decodable)=[]; %Delete from list of batches
                        obj.decoded(:,indexes)=B.x; %Add to list of decoded
                        
                        %Update indexes of decoded packets
                        obj.decoded_bool_indexes(indexes)=1;
                        obj.decoded_indexes=find(obj.decoded_bool_indexes);
                        
                        %Remove decoded packets from list of contributors 
                        %of other batches
                        zero_degree_batches=[];
                        for j=1:numel(obj.batches)
                            obj.batches(j).RemoveContributors(B.x,indexes);
                            if obj.batches(j).degree==0
                                zero_degree_batches=[zero_degree_batches, j];
                            end
                        end
                        obj.batches(zero_degree_batches)=[];
                        decodable=obj.FindOneDecodable();
                end
                
                
            else
            %If batch is not immediately decodable:
                %Remove previously eliminated contributors
                batch.RemoveContributors(obj.decoded,obj.decoded_indexes);
                %Check is now is resolvable
                if batch.isDecodable
                    obj.AddBatch(batch);
                else
                    %If it not resolvable, add to list of batches
                    if batch.degree~=0
                        obj.batches=[obj.batches;batch];
                    end
                end
            end
            
            %Is decoding completed?
            num_decoded=numel(obj.decoded_indexes);
            is_done=num_decoded>=obj.num_input_pkts;            
        end
    end
    
    methods(Access=private)
        function decodable=FindOneDecodable(obj)
            %%Find a decodable batch
            decodable=[];
            for i=1:numel(obj.batches)
                if obj.batches(i).isDecodable
                    decodable=i;
                    break;
                end
            end
        end
    end
end

