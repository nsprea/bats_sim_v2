classdef Batch < handle
    %% Batch object: the `packet' used in BATS coding.
    properties
        seed_id % Used both as index and as seed for PRNG
        degree % Number of contributors
        indexes % Index of contributors
        Gmat % Generator matrix
        Coeff %Coefficient matrix
        data % Encoded data matrix
        M % Batch-size
        rank %Rank of Gmat or data
    end
    
    methods
        function obj = Batch(M,seed_id,indexes,Gmat,Coeff,data)
            if nargin>0 && nargin <7
                obj.M=M;
                obj.seed_id=seed_id;
                obj.degree=size(indexes,2);
                obj.indexes=indexes;
                obj.Gmat=Gmat;
                obj.Coeff=Coeff;
                obj.data=data;
                obj.rank=rank(obj.Gmat);
            end
        end
        
        function tf=isDecodable(obj)
            %% Determine if batch degree equals its rank, so that the 
            % batch is decodable
            tf= obj.rank==obj.degree && obj.degree~=0;
        end
        
        function r=mtimes(obj1,obj2)
            %% Overload matrix product operator
            % Useful in many calculations
            
            data1=obj1;
            data2=obj2;
            Coeff1=obj1;
            Coeff2=obj2;
            
            if isa(obj1,'Batch')
                data1=obj1.data;
                Gmat=obj1.Gmat;
                Coeff1=obj1.Coeff;
                M=obj1.M;
                seed_id=obj1.seed_id;
                degree=obj1.degree;
                indexes=obj1.indexes;
            end
            if isa(obj2,'Batch')
                data2=obj2.data;
                Gmat=obj2.Gmat;
                Coeff2=obj2.Coeff;
                M=obj2.M;
                seed_id=obj2.seed_id;
                degree=obj2.degree;
                indexes=obj2.indexes;
            end
            
            data=data1*data2;
            Coeff=Coeff1*Coeff2;
            %Gmat=Gmat*Coeff;
            
            r=Batch(M,seed_id,indexes,Gmat,Coeff,data);
        end       
        
        function [B,indexes]=Decode(obj)
            %% Decode batch
            % It is assumed here that the batch is decodable.
            % This method will be called by the BP decoder.
            
            GH=double(obj.Gmat.x);
            Y=double(obj.data.x);
                        
            i=1;
            successful=0;
            curr_rank=0;
            
            GHr=zeros(obj.rank,obj.rank);
            Yr=zeros(size(Y,1),obj.rank);
            %% Here we select just the linearly independent columns
            for col=1:obj.M
                tmp(:,i)=[GH(:,col);Y(:,col)];
                %Compare rank computed adding a new column with curr_rank
                new_rank=rank(gf(tmp,obj.Gmat.m));
                if new_rank>curr_rank
                    %If rank is increased, then packet is innovative
                    curr_rank=new_rank; %Update rank
                    
                    %Save coeff. to compute solution
                    GHr(:,i)=GH(:,col); %Coefficients
                    Yr(:,i)=Y(:,col); %Data
                    
                    %Check for finish
                    %If the rank of the obtained matrix is at least equal 
                    %to the batch degree, we are done.
                    if curr_rank>=obj.degree
                        successful=1;
                        break;
                    end
                    %Update counter (otherwise new-line is overwritten)
                    i=i+1;
                end
            end
            %% Matrix inversion
            %If we were able to select at least obj.degree independent
            %columns, we are able to decode obj.degree packets
            if successful
                Yr=gf(Yr,obj.Gmat.m);
                GHr=gf(GHr,obj.Gmat.m);
                B=Yr/GHr; %Compute solution: B are the decoded packets
            end
            
            indexes=obj.indexes;%Index of decoded packets
            
            assert (size(indexes,2)==size(B,2)) %Check size
            B=gf(B,obj.Gmat.m); %Convert to GF
            
        end
               
        function tf=RemoveContributors(obj,B,list)
            %% Remove pkts in list form contributors of current batch
            
            %% Check if packets are contributors of this batch
            [ix,to_delete]=ismember(list,obj.indexes);
            %ix are boolean indexes
            %to_delete are their positions in obj.indexes
            if any(ix)
                %Position of indexes to delete from obj.indexes 
                to_delete=to_delete(ix); 
            
                B=double(B);
                
                %When using full list of received packets, undecoded columns
                %are set to zero so we need to remove them
                if size(B,2)~=size(list,2)
                    B=B(:,list);
                end

                m=obj.Gmat.m;%GF order
                Gtmp=obj.Gmat.x; %Temporary G matrix
                Dtmp=obj.data; %Temporary data matrix

                %%For each decoded packet to be removed from contributors
                %% Remove contribution of that packet in data matrix
                for i=to_delete
                    %If packet is contributor for this batch
                    pkt_no=obj.indexes(i);
                    
                    g=gf(Gtmp(i,:),m);
                    bj=B(:,list==pkt_no);
                    bj=gf(bj,m);
                    
                    Dtmp=Dtmp-bj*g; %Subtract from data matrix
                end
                %% Delete pkts from list of contributors
                obj.indexes(to_delete)=[];
                %% Delete corresponding row in G matrix
                Gtmp(to_delete,:)=[]; 
                %% Update degree
                obj.degree=numel(obj.indexes);

                %Convert to GF
                obj.Gmat=gf(Gtmp,m);
                obj.data=gf(Dtmp,m);

                obj.rank=rank(obj.Gmat);%Update rank
            end
        end
    end
end


