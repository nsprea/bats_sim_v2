% Author:           Nicolo' Sprea
% Title:            BATS simulator
% Version:          2.0
% Last revision:    April 5, 2019

clear all;
addpath('BATS_Code')
load Doppler50SNR15Thresh5.mat % <-----------------Erasure series file here

OUTPUT_FILENAME='2019.01.28.Batches.Doppler.250.SNR.15.Thresh.58.txt';

%% BCH code info
%=============
BCHThresh=5; %%Number of errors we can correct
BCHCWLength=1024; %Codeword length
BCHMsgLength=log2(BCHCWLength);
k=BCHCWLength-1-BCHMsgLength*BCHThresh;
CR_bch=k/BCHCWLength; %Code rate BCH

%% Parameters
%============
PktSize=64;   %Packet size
NumPkts=1024 ;   %Number of input packets
GFOrder1=8;   %Order of GF for outer code
GFOrder2=1;   %Order of GF for inner code
ICDensity=0.5; %Inner code density

FID=fopen(OUTPUT_FILENAME,'a');
fprintf(FID,'M\tRelays\tGptBats\tCR_bch\tGpt\n');
for M=[4 8 16] % M is the Batch size (set =1 for fountain coding)
    for NumRelays=1:10
        clear BATS_Code
        %T=64;
        ErIndex=1;
        %-----------------------------------

        fprintf('***** Simulation starts *****\n');
        fprintf(' . Input data:\t\t%dX%d matrix,\n',PktSize,NumPkts);
        fprintf(' . Batch size M:\t%d\n',M);
        fprintf(' . Number of relays:\t%d\n',NumRelays);

        %% Input data
        rng('shuffle','twister');
        InputData=randi([0 2^GFOrder1-1],PktSize,NumPkts);

        %Define a BATS Code object
        BC = BATS_Code.instance(NumPkts,M,GFOrder1,GFOrder2,ICDensity);

        DecodedFlag=0;
        fprintf('Transmitting... ');
        fprintf('0000 batches');

        NumTxBatches=0;
        DecFailed=0;
        tic
        while DecodedFlag==0 && ~DecFailed
            NumTxBatches=NumTxBatches+1;
            fprintf('\b\b\b\b\b\b\b\b\b\b\b\b%4d batches', NumTxBatches);
            %% Encoding
            EncBatch=BC.Encode(InputData);
            %% Propagation over channel
            ErIndex=mod(M*11*(NumTxBatches-1)+1,100000);
            for link=1:NumRelays
              ErasMat=LongSeq(ErIndex:(ErIndex+M-1));
              ErIndex=mod(ErIndex+M,100000)+1;
              ErasMat=diag(ErasMat);
              EncBatch=EncBatch*ErasMat;

              EncBatch=BC.Recode(EncBatch);%Recode <-- Comment this line to avoid recoding
            end
            %Last link
            ErasMat=LongSeq(ErIndex:(ErIndex+M-1));
            ErIndex=mod(ErIndex+M,100000)+1;
            ErasMat=diag(ErasMat);
            EncBatch=EncBatch*ErasMat;

            %% Decoding
            DecodedFlag=BC.Decode(EncBatch);

            %If rate is less than 1e-2 decoding is failed
            if NumPkts/(NumTxBatches*M)<1e-2
                DecFailed=1;
            end

        end

        fprintf('\b\b\b\b\b\b\b\b\b\b\b\bReceived. (%.2f seconds)\n',toc);
        fprintf('Used %d batches (%d packets).\n',NumTxBatches,NumTxBatches*M);
        
        %% *** OUTPUT ***         
        CR_bats=1024./(NumTxBatches.*M);
        Pkt_OH=(16+M)./(16+M+64*8);

        GPT_bats=CR_bats.*(1-Pkt_OH);
        GPT=CR_bch.*GPT_bats;

        fprintf(FID,'%d\t%d\t%6.4f\t%6.4f\t%6.4f\n',M,NumRelays,GPT_bats,CR_bch,GPT);
        fprintf('***** Simulation ended *****\n');
    end
 end
 fclose(FID);
